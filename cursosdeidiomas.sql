-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2020 a las 19:10:00
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cursosdeidiomas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `rut` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `id_Cuenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `rut`, `nombre`, `id_Cuenta`) VALUES
(1, '112223334', 'admin', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `id` int(10) NOT NULL,
  `rut` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido_Paterno` varchar(255) DEFAULT NULL,
  `apellido_Materno` varchar(255) DEFAULT NULL,
  `id_Curso` int(10) DEFAULT NULL,
  `id_Cuenta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `rut`, `nombre`, `apellido_Paterno`, `apellido_Materno`, `id_Curso`, `id_Cuenta`) VALUES
(20, '234536758', 'angel', 'Garrido', 'Garrido', 3, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nivel_Usuario` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id`, `usuario`, `password`, `nivel_Usuario`) VALUES
(5, '112223334', '112223334', 1),
(29, '234536758', '234536758', 3),
(30, 'hernandez', 'hernandez', 2),
(31, '23434564', '23434564', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `id` int(10) NOT NULL,
  `nivel` varchar(255) DEFAULT NULL,
  `cantidad_Actual` int(10) DEFAULT NULL,
  `cantidad_Maxima` int(10) DEFAULT NULL,
  `id_Idioma` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`id`, `nivel`, `cantidad_Actual`, `cantidad_Maxima`, `id_Idioma`) VALUES
(1, 'Principiante', 0, 30, 1),
(2, 'Elemental', 0, 30, 1),
(3, 'Pre Intermedio', 1, 30, 1),
(4, 'Intermedio', 0, 30, 1),
(5, 'Intermedio Avanzado', 0, 30, 1),
(6, 'Avanzado', 0, 30, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE `idioma` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`id`, `nombre`) VALUES
(1, 'Ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `id` int(10) NOT NULL,
  `rut` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `id_Curso` int(10) DEFAULT NULL,
  `id_Cuenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`id`, `rut`, `nombre`, `id_Curso`, `id_Cuenta`) VALUES
(9, 'hernandez', 'Daniela', 1, 30),
(10, '23434564', 'Pablo Iturra', NULL, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `spring_session`
--

CREATE TABLE `spring_session` (
  `PRIMARY_ID` char(36) NOT NULL,
  `SESSION_ID` char(36) NOT NULL,
  `CREATION_TIME` bigint(20) NOT NULL,
  `LAST_ACCESS_TIME` bigint(20) NOT NULL,
  `MAX_INACTIVE_INTERVAL` int(11) NOT NULL,
  `EXPIRY_TIME` bigint(20) NOT NULL,
  `PRINCIPAL_NAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `spring_session`
--

INSERT INTO `spring_session` (`PRIMARY_ID`, `SESSION_ID`, `CREATION_TIME`, `LAST_ACCESS_TIME`, `MAX_INACTIVE_INTERVAL`, `EXPIRY_TIME`, `PRINCIPAL_NAME`) VALUES
('8333e8fc-3c84-405b-a757-39486648b6dc', '3d2deab4-d4e7-4278-b61e-663d2ab9aaad', 1592931653896, 1592931660691, 1800, 1592933460691, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `spring_session_attributes`
--

CREATE TABLE `spring_session_attributes` (
  `SESSION_PRIMARY_ID` char(36) NOT NULL,
  `ATTRIBUTE_NAME` varchar(200) NOT NULL,
  `ATTRIBUTE_BYTES` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `spring_session_attributes`
--

INSERT INTO `spring_session_attributes` (`SESSION_PRIMARY_ID`, `ATTRIBUTE_NAME`, `ATTRIBUTE_BYTES`) VALUES
('8333e8fc-3c84-405b-a757-39486648b6dc', 'usuarioLogueado', 0xaced000573720028636c2e6463692e7566726f2e637572736f7344654964696f6d61732e6d6f64656c2e4375656e746100000000000000010200074c000961646d696e4c6973747400104c6a6176612f7574696c2f4c6973743b4c000a616c756d6e6f4c69737471007e00014c000269647400134c6a6176612f6c616e672f496e74656765723b4c000c6e6976656c5573756172696f71007e00024c000870617373776f72647400124c6a6176612f6c616e672f537472696e673b4c000c70726f6665736f724c69737471007e00014c00077573756172696f71007e000378707372002f6f72672e68696265726e6174652e636f6c6c656374696f6e2e696e7465726e616c2e50657273697374656e74426167fe57c5afda4fa6440200024c000362616771007e00014c001270726f7669646564436f6c6c656374696f6e7400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7872003e6f72672e68696265726e6174652e636f6c6c656374696f6e2e696e7465726e616c2e416273747261637450657273697374656e74436f6c6c656374696f6e5718b75d8aba735402000b5a001b616c6c6f774c6f61644f7574736964655472616e73616374696f6e49000a63616368656453697a655a000564697274795a000e656c656d656e7452656d6f7665645a000b696e697469616c697a65645a000d697354656d7053657373696f6e4c00036b65797400164c6a6176612f696f2f53657269616c697a61626c653b4c00056f776e65727400124c6a6176612f6c616e672f4f626a6563743b4c0004726f6c6571007e00034c001273657373696f6e466163746f72795575696471007e00034c000e73746f726564536e617073686f7471007e0008787000ffffffff00000100737200116a6176612e6c616e672e496e746567657212e2a0a4f781873802000149000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b02000078700000000571007e0004740032636c2e6463692e7566726f2e637572736f7344654964696f6d61732e6d6f64656c2e4375656e74612e61646d696e4c69737470737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a6578700000000177040000000173720027636c2e6463692e7566726f2e637572736f7344654964696f6d61732e6d6f64656c2e41646d696e00000000000000010200044c0002696471007e00024c000869644375656e746174002a4c636c2f6463692f7566726f2f637572736f7344654964696f6d61732f6d6f64656c2f4375656e74613b4c00066e6f6d62726571007e00034c000372757471007e000378707371007e000b0000000171007e000474000561646d696e740009313132323233333334787371007e000f0000000177040000000171007e001378707371007e000500ffffffff0000000071007e000d71007e0004740033636c2e6463692e7566726f2e637572736f7344654964696f6d61732e6d6f64656c2e4375656e74612e616c756d6e6f4c6973747070707071007e000d71007e00147400093131323232333333347371007e000500ffffffff0000000071007e000d71007e0004740035636c2e6463692e7566726f2e637572736f7344654964696f6d61732e6d6f64656c2e4375656e74612e70726f6665736f724c69737470707070740009313132323233333334);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5ya03fnbqodo762w5c1cfuxu3` (`id_Cuenta`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5hnjffm1qukfw445asiwkowtn` (`id_Cuenta`),
  ADD KEY `FKai7w6ro86sq58nbm0qf3gyckf` (`id_Curso`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcurso921919` (`id_Idioma`);

--
-- Indices de la tabla `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKmjhktq85pjveg7vpqlvmwb0ii` (`id_Cuenta`),
  ADD KEY `FKi66nkaxg4bnr6u1be1wldqunj` (`id_Curso`);

--
-- Indices de la tabla `spring_session`
--
ALTER TABLE `spring_session`
  ADD PRIMARY KEY (`PRIMARY_ID`),
  ADD UNIQUE KEY `SPRING_SESSION_IX1` (`SESSION_ID`),
  ADD KEY `SPRING_SESSION_IX2` (`EXPIRY_TIME`),
  ADD KEY `SPRING_SESSION_IX3` (`PRINCIPAL_NAME`);

--
-- Indices de la tabla `spring_session_attributes`
--
ALTER TABLE `spring_session_attributes`
  ADD PRIMARY KEY (`SESSION_PRIMARY_ID`,`ATTRIBUTE_NAME`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `idioma`
--
ALTER TABLE `idioma`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `FK5ya03fnbqodo762w5c1cfuxu3` FOREIGN KEY (`id_Cuenta`) REFERENCES `cuenta` (`id`),
  ADD CONSTRAINT `FKadmin173086` FOREIGN KEY (`id_Cuenta`) REFERENCES `cuenta` (`id`);

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `FK5hnjffm1qukfw445asiwkowtn` FOREIGN KEY (`id_Cuenta`) REFERENCES `cuenta` (`id`),
  ADD CONSTRAINT `FKai7w6ro86sq58nbm0qf3gyckf` FOREIGN KEY (`id_Curso`) REFERENCES `curso` (`id`),
  ADD CONSTRAINT `FKalumno102736` FOREIGN KEY (`id_Cuenta`) REFERENCES `cuenta` (`id`),
  ADD CONSTRAINT `FKalumno786159` FOREIGN KEY (`id_Curso`) REFERENCES `curso` (`id`);

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `FK566dr976d5fcpoiwak7xjd7m7` FOREIGN KEY (`id_Idioma`) REFERENCES `idioma` (`id`),
  ADD CONSTRAINT `FKcurso921919` FOREIGN KEY (`id_Idioma`) REFERENCES `idioma` (`id`);

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `FKi66nkaxg4bnr6u1be1wldqunj` FOREIGN KEY (`id_Curso`) REFERENCES `curso` (`id`),
  ADD CONSTRAINT `FKmjhktq85pjveg7vpqlvmwb0ii` FOREIGN KEY (`id_Cuenta`) REFERENCES `cuenta` (`id`),
  ADD CONSTRAINT `FKprofesor555885` FOREIGN KEY (`id_Curso`) REFERENCES `curso` (`id`),
  ADD CONSTRAINT `FKprofesor872461` FOREIGN KEY (`id_Cuenta`) REFERENCES `cuenta` (`id`);

--
-- Filtros para la tabla `spring_session_attributes`
--
ALTER TABLE `spring_session_attributes`
  ADD CONSTRAINT `SPRING_SESSION_ATTRIBUTES_FK` FOREIGN KEY (`SESSION_PRIMARY_ID`) REFERENCES `spring_session` (`PRIMARY_ID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
