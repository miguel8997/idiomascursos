package cl.dci.ufro.cursosDeIdiomas;

import cl.dci.ufro.cursosDeIdiomas.logica.FuncionesAlumno;
import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import cl.dci.ufro.cursosDeIdiomas.model.Curso;
import cl.dci.ufro.cursosDeIdiomas.services.CursoService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CursosDeIdiomasApplicationTests {

    @Autowired
    private CursoService cursoService;
    
    @Autowired
    private FuncionesAlumno funcAlum;
    
    @Test
    void contextLoads() {

        List<Curso> cursos=cursoService.listarTodos();
        
        Alumno a = new Alumno();
        a.setNombre("carlos");
        a.setApellidoPaterno("Perez");
        a.setApellidoMaterno("Iturra");
        a.setIdCurso(cursos.get(0));
        
        funcAlum.agregarAlumno(a);

    }

}
