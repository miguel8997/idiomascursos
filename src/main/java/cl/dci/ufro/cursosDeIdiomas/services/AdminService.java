
package cl.dci.ufro.cursosDeIdiomas.services;

import cl.dci.ufro.cursosDeIdiomas.model.Admin;
import java.util.List;


public interface AdminService {
    
    public List<Admin> listarTodos();
    public void Guardar(Admin admin);
    public Admin buscarPorId(Integer id);
}
