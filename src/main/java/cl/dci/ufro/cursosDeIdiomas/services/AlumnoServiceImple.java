package cl.dci.ufro.cursosDeIdiomas.services;


import cl.dci.ufro.cursosDeIdiomas.dao.AlumnoDao;
import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlumnoServiceImple implements AlumnoService {

    @Autowired
    private AlumnoDao alumnoDao;

    @Override
    public List<Alumno> listarTodos() {
        return alumnoDao.findAll();
    }

    @Override
    public void Guardar(Alumno alumno) {
        alumnoDao.save(alumno);
    }

    @Override
    public Alumno buscarPorId(Integer id) {
        return alumnoDao.findById(id).orElse(null);
    }

}
