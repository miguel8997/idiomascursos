
package cl.dci.ufro.cursosDeIdiomas.services;

import cl.dci.ufro.cursosDeIdiomas.model.Profesor;
import java.util.List;


public interface ProfesorService {
    
    public List<Profesor> listarTodos();
    public void Guardar(Profesor profesor);
    public Profesor buscarPorId(Integer id);
}
