
package cl.dci.ufro.cursosDeIdiomas.services;

import cl.dci.ufro.cursosDeIdiomas.dao.AdminDao;
import cl.dci.ufro.cursosDeIdiomas.model.Admin;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImple implements AdminService {

    @Autowired
    private AdminDao adminDao;

    @Override
    public List<Admin> listarTodos() {
        return adminDao.findAll();
    }

    @Override
    public void Guardar(Admin admin) {
        adminDao.save(admin);
    }

    @Override
    public Admin buscarPorId(Integer id) {
        return adminDao.findById(id).orElse(null);
    }
    
}
