
package cl.dci.ufro.cursosDeIdiomas.services;


import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import java.util.List;


public interface AlumnoService {
    
    public List<Alumno> listarTodos();
    public void Guardar(Alumno alumno);
    public Alumno buscarPorId(Integer id); 
}
