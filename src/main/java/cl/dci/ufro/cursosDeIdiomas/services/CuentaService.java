
package cl.dci.ufro.cursosDeIdiomas.services;

import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import java.util.List;


public interface CuentaService {
    
    public List<Cuenta> listarTodos();
    public void Guardar(Cuenta cuenta);
    public Cuenta buscarPorId(Integer id);
    public Cuenta login(String user,String pass);
}
