
package cl.dci.ufro.cursosDeIdiomas.services;


import cl.dci.ufro.cursosDeIdiomas.dao.CursoDao;
import cl.dci.ufro.cursosDeIdiomas.model.Curso;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoServiceImple implements CursoService {

    @Autowired
    private CursoDao cursoDao;

    @Override
    public List<Curso> listarTodos() {
        return cursoDao.findAll();
    }

    @Override
    public void Guardar(Curso curso) {
        cursoDao.save(curso);
    }

    @Override
    public Curso buscarPorId(Integer id) {
        return cursoDao.findById(id).orElse(null);
    }
    
    
    
}
