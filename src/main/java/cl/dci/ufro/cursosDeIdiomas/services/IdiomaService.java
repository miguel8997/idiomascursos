
package cl.dci.ufro.cursosDeIdiomas.services;


import cl.dci.ufro.cursosDeIdiomas.model.Idioma;
import java.util.List;


public interface IdiomaService {
    
    public List<Idioma> listarTodos();
    public void Guardar(Idioma idioma);
    public Idioma buscarPorId(Integer id); 
}
