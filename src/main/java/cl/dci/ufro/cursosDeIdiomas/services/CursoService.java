
package cl.dci.ufro.cursosDeIdiomas.services;


import cl.dci.ufro.cursosDeIdiomas.model.Curso;
import java.util.List;


public interface CursoService {
    
    public List<Curso> listarTodos();
    public void Guardar(Curso curso);
    public Curso buscarPorId(Integer id); 
}
