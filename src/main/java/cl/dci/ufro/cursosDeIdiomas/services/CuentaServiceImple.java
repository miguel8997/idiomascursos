
package cl.dci.ufro.cursosDeIdiomas.services;

import cl.dci.ufro.cursosDeIdiomas.dao.CuentaDao;
import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CuentaServiceImple implements CuentaService {
    
    @Autowired
    private CuentaDao cuentaDao;

    @Override
    public List<Cuenta> listarTodos() {
        return cuentaDao.findAll();
    }

    @Override
    public void Guardar(Cuenta cuenta) {
        cuentaDao.save(cuenta);
    }

    @Override
    public Cuenta buscarPorId(Integer id) {
        return cuentaDao.findById(id).orElse(null);
    }

    @Override
    public Cuenta login(String user,String pass) {
        return cuentaDao.findByUsuarioAndPassword(user, pass);
    }
}
