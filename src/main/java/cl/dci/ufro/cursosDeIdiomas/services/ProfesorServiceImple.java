
package cl.dci.ufro.cursosDeIdiomas.services;

import cl.dci.ufro.cursosDeIdiomas.dao.ProfesorDao;
import cl.dci.ufro.cursosDeIdiomas.model.Profesor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfesorServiceImple implements ProfesorService {
    
    @Autowired
    private ProfesorDao profesorDao;

    @Override
    public List<Profesor> listarTodos() {
        return profesorDao.findAll();
    }

    @Override
    public void Guardar(Profesor profesor) {
        profesorDao.save(profesor);
    }

    @Override
    public Profesor buscarPorId(Integer id) {
        return profesorDao.findById(id).orElse(null);
    }
}
