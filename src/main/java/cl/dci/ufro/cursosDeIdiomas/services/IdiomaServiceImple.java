
package cl.dci.ufro.cursosDeIdiomas.services;


import cl.dci.ufro.cursosDeIdiomas.dao.IdiomaDao;
import cl.dci.ufro.cursosDeIdiomas.model.Idioma;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdiomaServiceImple implements IdiomaService {

    @Autowired
    private IdiomaDao idiomaDao;
    
    @Override
    public List<Idioma> listarTodos() {
       return idiomaDao.findAll();
    }

    @Override
    public void Guardar(Idioma idioma) {
        idiomaDao.save(idioma);
    }

    @Override
    public Idioma buscarPorId(Integer id) {
        return idiomaDao.findById(id).orElse(null);
    }
    
}
