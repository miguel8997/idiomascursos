
package cl.dci.ufro.cursosDeIdiomas.controller;

import cl.dci.ufro.cursosDeIdiomas.logica.FuncionesIdioma;
import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import cl.dci.ufro.cursosDeIdiomas.model.Idioma;
import cl.dci.ufro.cursosDeIdiomas.services.AlumnoService;
import cl.dci.ufro.cursosDeIdiomas.services.IdiomaService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IdiomaController {
    
    @Autowired
    private IdiomaService idiomaService;
    
    @Autowired
    private FuncionesIdioma funcIdiomas;
    
    @Autowired
    private AlumnoService alumnoService;
    
    @GetMapping("/idiomas")
    public String mostrarIdiomas(Model model, HttpServletRequest request){
        
        List<Idioma> idiomas=idiomaService.listarTodos();
        
        Cuenta cuenta=(Cuenta)request.getSession().getAttribute("usuarioLogueado");
        if (cuenta.getNivelUsuario()==3) {
            Alumno alumnoLogueado=alumnoService.buscarPorId(cuenta.getAlumnoList().get(0).getId());
            model.addAttribute("alumnoLogueado", alumnoLogueado);
        }
        
        
        model.addAttribute("titulo", "Lista de idiomas");
        model.addAttribute("idiomas", idiomas);
        
        
        return "mostrarIdiomas";
    }
    
    
    @GetMapping("/idioma/{idIdioma}/verCursos")
    public String verIdiomas(@PathVariable(name="idIdioma") Integer id,Model model , HttpServletRequest request){
        
        Cuenta cuenta=(Cuenta)request.getSession().getAttribute("usuarioLogueado");
        
        Idioma i=idiomaService.buscarPorId(id.intValue());
        
        funcIdiomas.actualizarCantidadActualDeAlumnosPorCurso(i);
        
        if (cuenta.getNivelUsuario()==3) {
            Alumno alumnoLogueado=alumnoService.buscarPorId(cuenta.getAlumnoList().get(0).getId());
            model.addAttribute("alumnoLogueado", alumnoLogueado);
        }
        
        model.addAttribute("idioma", i);
        model.addAttribute("titulo", "Cursos de");
        
        return "mostrarCursos";
    }
    
    @GetMapping("/idiomas/agregarIdioma")
    public String agregarIdioma(Model model){
        
        model.addAttribute("titulo", "Creando nuevo idioma");
        model.addAttribute("nuevoIdioma", new Idioma());
        
        return "agregarIdioma";
    }
    
    @PostMapping("/idiomas/agregarIdioma")
    public String agregarIdioma(@ModelAttribute Idioma idioma){
        
        
        idiomaService.Guardar(idioma);
        
        List<Idioma> idiomas= idiomaService.listarTodos();
        
 
        
        funcIdiomas.agregarCursosAutomaticamente(); 
        
        return "redirect:/idiomas";
    }
}
