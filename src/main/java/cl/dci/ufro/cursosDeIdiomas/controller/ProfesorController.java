
package cl.dci.ufro.cursosDeIdiomas.controller;

import cl.dci.ufro.cursosDeIdiomas.logica.FuncionesProfesor;
import cl.dci.ufro.cursosDeIdiomas.model.Profesor;
import cl.dci.ufro.cursosDeIdiomas.services.ProfesorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ProfesorController {
    
    @Autowired
    private ProfesorService profesorService;
    
    @Autowired
    private FuncionesProfesor funcProfesor;
    
    @GetMapping("/mostrarProfesores")
    public String mostrarProfesores(Model model){
  
        List<Profesor> profesores=profesorService.listarTodos();
        
        model.addAttribute("titulo", "Lista de profesores");
        model.addAttribute("profesores",profesores);
        
        return "mostrarProfesores";
    }
    
    @GetMapping("/registrarNuevoProfesor")
    public String agregarProfesor(Model model){
  
        model.addAttribute("registrarProfesor", new Profesor());
        
        return "registrarNuevoProfesor";
    }
    
    @PostMapping("/registrarNuevoProfesor")
    public String agregarProfesor(@ModelAttribute Profesor s){
        
        funcProfesor.agregarCuentaAutomaticamenteAlProfesor(s);
        
        profesorService.Guardar(s);

        return "redirect:/mostrarProfesores";
    }
    
    @GetMapping("/editarProfesor/{id}") 
    public String editarProfesor( @PathVariable("id") Integer idProfesor,Model model) {
        
        Profesor profesor=profesorService.buscarPorId(idProfesor);
        

        model.addAttribute("editarProfesor", profesor);

        return "editarProfesor";

    }
    
    @PostMapping("/editarProfesor")
    public String editarProfesor(@ModelAttribute Profesor p){
        
        profesorService.Guardar(p);

        return "redirect:/mostrarProfesores";
    }
}
