package cl.dci.ufro.cursosDeIdiomas.controller;

import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import cl.dci.ufro.cursosDeIdiomas.model.Curso;
import cl.dci.ufro.cursosDeIdiomas.model.Profesor;
import cl.dci.ufro.cursosDeIdiomas.services.AlumnoService;
import cl.dci.ufro.cursosDeIdiomas.services.CuentaService;
import cl.dci.ufro.cursosDeIdiomas.services.CursoService;
import cl.dci.ufro.cursosDeIdiomas.services.ProfesorService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class CursoController {
    
    @Autowired
    private CursoService cursoService;
    
    @Autowired
    private AlumnoService alumnoService;
    
    @Autowired
    private ProfesorService profesorService;
    
    @Autowired
    private CuentaService cuentaService;
   
    @GetMapping("/curso/{idCurso}/verAlumnos")
    public String verCursos(@PathVariable(name="idCurso") Integer id,Model model, HttpServletRequest request){
        int acum=0;
        List<Profesor> profesores=profesorService.listarTodos();
        
        for (int i = 0; i < profesores.size(); i++) {
            if (profesores.get(i).getIdCurso()==null) {
                acum++;
            }
        }
        
        Cuenta cuenta=(Cuenta)request.getSession().getAttribute("usuarioLogueado");
        
        Curso c=cursoService.buscarPorId(id.intValue());
        
        if (cuenta.getNivelUsuario()==3) {
            Alumno alumnoLogueado=alumnoService.buscarPorId(cuenta.getAlumnoList().get(0).getId());
            model.addAttribute("alumnoLogueado", alumnoLogueado);
        }
        
        model.addAttribute("curso", c);
        model.addAttribute("profesoresSinCurso", acum);
        
        return "verCurso";
    }
    
    @GetMapping("/sinCurso")
    public String profesorSinCurso(Model model, HttpServletRequest request){
        
        
        model.addAttribute("titulo","Sin curso por el momento");
        
        return "profesorSinCurso";
    }
    
    @GetMapping("/curso/{idCurso}/agregarAlumnoAlCurso")
    public String agregarAlumno(@PathVariable(name="idCurso") Integer id,Model model, HttpServletRequest request){
        
        Cuenta cuenta=(Cuenta)request.getSession().getAttribute("usuarioLogueado");
        
        Curso c=cursoService.buscarPorId(id.intValue());
        
        if (cuenta.getNivelUsuario()==3) {
            Alumno alumnoLogueado=alumnoService.buscarPorId(cuenta.getAlumnoList().get(0).getId());
            model.addAttribute("alumnoLogueado", alumnoLogueado);
        }
        model.addAttribute("curso", c);
        model.addAttribute("nuevoAlumno", new Alumno());
        
        return "agregarAlumnoAlCurso";
    }
    
    @PostMapping("/curso/{idCurso}/agregarAlumnoAlCurso")
    public String agregarAlumno(@PathVariable(name="idCurso") Integer id,@ModelAttribute Alumno a){
        
        Alumno alumno=alumnoService.buscarPorId(a.getId());
        Curso c=cursoService.buscarPorId(id.intValue());
        alumno.setIdCurso(c);
        
        alumnoService.Guardar(alumno);
        return "redirect:/curso/"+id.toString()+"/verAlumnos";
    }
    
    @GetMapping("/curso/{idCurso}/agregarProfesorAlCurso")
    public String agregarProfesor(@PathVariable(name="idCurso") Integer id,Model model){
        
        List<Profesor> profesores=profesorService.listarTodos();
        
        Curso c=cursoService.buscarPorId(id.intValue());
        
        model.addAttribute("curso", c);
        model.addAttribute("nuevoProfesor", new Profesor());
        model.addAttribute("profesores",profesores);
        
        return "agregarProfesorAlCurso";
    }
    
    @PostMapping("/curso/{idCurso}/agregarProfesorAlCurso")
    public String agregarProfesor(@PathVariable(name="idCurso") Integer id,@ModelAttribute Profesor p){
        
        Profesor profesor=profesorService.buscarPorId(p.getId());
        Curso c=cursoService.buscarPorId(id.intValue());
        profesor.setIdCurso(c);
        
        profesorService.Guardar(profesor);

        
        return "redirect:/curso/"+id.toString()+"/verAlumnos";
    }
    
}
