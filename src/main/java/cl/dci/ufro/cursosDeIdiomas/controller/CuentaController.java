package cl.dci.ufro.cursosDeIdiomas.controller;

import cl.dci.ufro.cursosDeIdiomas.model.Admin;
import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import cl.dci.ufro.cursosDeIdiomas.model.Profesor;
import cl.dci.ufro.cursosDeIdiomas.services.CuentaService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class CuentaController {

    @Autowired
    private CuentaService cuentaService;

    @GetMapping("/login")
    public String Login(Model model) {

        model.addAttribute("cuenta", new Cuenta());

        return "login";
    }

    @PostMapping("/login")
    public String Login(Model model,@ModelAttribute Cuenta c, HttpServletRequest request) {
        
        Alumno alumno=null;
        Profesor profe = null;
        Admin admin=null;
        
        Cuenta usuarioBD = cuentaService.login(c.getUsuario(), c.getPassword());
        

        if (usuarioBD != null) {
            if (usuarioBD.getNivelUsuario()==3) {
                alumno=usuarioBD.getAlumnoList().get(0);
            }else if(usuarioBD.getNivelUsuario()==2){
                profe=usuarioBD.getProfesorList().get(0);
            }else{
                admin=usuarioBD.getAdminList().get(0);
            } 
            
            
            request.getSession().setAttribute("usuarioLogueado", usuarioBD);


            Cuenta cuenta=(Cuenta)request.getSession().getAttribute("usuarioLogueado");
            
            if (cuenta.getNivelUsuario()==3) {
                return "redirect:/idiomas";
            }else if(cuenta.getNivelUsuario()==2&&cuenta.getProfesorList().get(0).getIdCurso()==null){ 
                    return "redirect:/sinCurso";
            }else if(cuenta.getNivelUsuario()==2&&cuenta.getProfesorList().get(0).getIdCurso()!=null){
                return "redirect:/curso/"+profe.getIdCurso().getId()+"/verAlumnos";
            }else{
                return "redirect:/idiomas";
            } 
            
        } else {
            model.addAttribute("cuenta", new Cuenta());
            model.addAttribute("error",true);
            return "login";
        }

    }
    
    @GetMapping("/logout")
    public String logout(HttpServletRequest request,Model model) {

        
        return "logout";

    }
    
    @PostMapping("/logout")
    public String logout(HttpServletRequest request) {

        request.getSession().invalidate();
        return "redirect:/login";

    }
}
