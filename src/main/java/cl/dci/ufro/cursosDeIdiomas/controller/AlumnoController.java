
package cl.dci.ufro.cursosDeIdiomas.controller;

import cl.dci.ufro.cursosDeIdiomas.logica.FuncionesAlumno;
import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import cl.dci.ufro.cursosDeIdiomas.services.AlumnoService;
import cl.dci.ufro.cursosDeIdiomas.services.CuentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class AlumnoController {
    
    @Autowired
    private AlumnoService alumnoService;
    
    
    @Autowired
    private FuncionesAlumno funcAlumno;
    
    @GetMapping("/mostrarAlumnos")
    public String mostrarAlumnos(Model model){
  
        List<Alumno> alumnos=alumnoService.listarTodos();
        
        model.addAttribute("titulo", "Lista de alumnos");
        model.addAttribute("alumnos",alumnos);
        
        return "mostrarAlumnos";
    }
    
    @GetMapping("/registrarNuevoAlumno")
    public String agregarAlumno(Model model){
  
        model.addAttribute("registrarAlumno", new Alumno());
        
        return "registrarNuevoAlumno";
    }
    
    @PostMapping("/registrarNuevoAlumno")
    public String agregarAlumno(@ModelAttribute Alumno a){
        
        funcAlumno.agregarCuentaAutomaticamenteAlAlumno(a);
        
        alumnoService.Guardar(a);

        return "redirect:/mostrarAlumnos";
    }
    
    @GetMapping("/editarAlumno/{id}") 
    public String editarAlumno( @PathVariable("id") Integer idAlumno,Model model) {
        
        Alumno alumno=alumnoService.buscarPorId(idAlumno);
        

        model.addAttribute("editarAlumno", alumno);

        return "editarAlumno";

    }
    
    @PostMapping("/editarAlumno")
    public String editarAlumno(@ModelAttribute Alumno a){
        
        alumnoService.Guardar(a);

        return "redirect:/mostrarAlumnos";
    }
}
