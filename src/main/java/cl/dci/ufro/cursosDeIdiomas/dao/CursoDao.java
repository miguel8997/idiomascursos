
package cl.dci.ufro.cursosDeIdiomas.dao;


import cl.dci.ufro.cursosDeIdiomas.model.Curso;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface CursoDao extends CrudRepository<Curso, Integer> {
    @Override
    public List<Curso> findAll();
}
