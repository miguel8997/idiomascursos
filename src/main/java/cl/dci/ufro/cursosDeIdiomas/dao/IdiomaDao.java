
package cl.dci.ufro.cursosDeIdiomas.dao;


import cl.dci.ufro.cursosDeIdiomas.model.Idioma;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface IdiomaDao extends CrudRepository<Idioma, Integer>{
    
    @Override
    public List<Idioma> findAll();
}
