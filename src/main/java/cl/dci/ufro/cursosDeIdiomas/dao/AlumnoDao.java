/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dci.ufro.cursosDeIdiomas.dao;


import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface AlumnoDao extends CrudRepository<Alumno,Integer> {
    @Override
    public List<Alumno> findAll();
}
