/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dci.ufro.cursosDeIdiomas.dao;

import cl.dci.ufro.cursosDeIdiomas.model.Admin;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface AdminDao extends CrudRepository<Admin,Integer> {
    
    @Override
    public List<Admin> findAll();
}
