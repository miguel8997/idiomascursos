/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dci.ufro.cursosDeIdiomas.logica;

import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import cl.dci.ufro.cursosDeIdiomas.model.Profesor;
import cl.dci.ufro.cursosDeIdiomas.services.CuentaService;
import cl.dci.ufro.cursosDeIdiomas.services.ProfesorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FuncionesProfesor {
    
    @Autowired
    private ProfesorService profesorService;
    
    @Autowired
    private CuentaService cuenService;

    public FuncionesProfesor() {
    }
    
    public void agregarCuentaAutomaticamenteAlProfesor(Profesor p){

        Cuenta c=new Cuenta();
        c.setUsuario(p.getRut());
        c.setPassword(p.getRut());
        c.setNivelUsuario(2);
        cuenService.Guardar(c); 
        
        List<Cuenta> cuentas=cuenService.listarTodos();
        
        p.setIdCuenta(cuentas.get(cuentas.size()-1));
        
    }
}
