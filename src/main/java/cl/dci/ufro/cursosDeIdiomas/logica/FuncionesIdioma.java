package cl.dci.ufro.cursosDeIdiomas.logica;

import cl.dci.ufro.cursosDeIdiomas.model.Curso;
import cl.dci.ufro.cursosDeIdiomas.model.Idioma;
import cl.dci.ufro.cursosDeIdiomas.services.CursoService;
import cl.dci.ufro.cursosDeIdiomas.services.IdiomaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FuncionesIdioma {

    @Autowired
    private IdiomaService idiomaService;
    
    @Autowired
    private CursoService cursoService;

    public FuncionesIdioma() {
    }

    public void agregarCursosAutomaticamente() {

        List<Idioma> idiomas= idiomaService.listarTodos();
        
        Idioma i = idiomaService.buscarPorId(idiomas.get(idiomas.size()-1).getId());
      
        String[] nivel={"Principiante","Elemental","Pre Intermedio","Intermedio","Intermedio Avanzado","Avanzado"};
        
        for (int j = 0; j < nivel.length; j++) {
            Curso c=new Curso();
            c.setNivel(nivel[j]);
            c.setCantidadActual(0);
            c.setCantidadMaxima(30);
            c.setIdIdioma(i);
            cursoService.Guardar(c);
        } 

    }
    
    public void actualizarCantidadActualDeAlumnosPorCurso(Idioma idioma) {

        
        
        for (int i = 0; i < idioma.getCursoList().size(); i++) {
            idioma.getCursoList().get(i).setCantidadActual(idioma.getCursoList().get(i).getAlumnoList().size());
        }
        
        idiomaService.Guardar(idioma);

    }

}
