
package cl.dci.ufro.cursosDeIdiomas.logica;

import cl.dci.ufro.cursosDeIdiomas.model.Alumno;
import cl.dci.ufro.cursosDeIdiomas.model.Cuenta;
import cl.dci.ufro.cursosDeIdiomas.services.AlumnoService;
import cl.dci.ufro.cursosDeIdiomas.services.CuentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FuncionesAlumno {
    
    @Autowired
    private AlumnoService alumService;
    
    @Autowired
    private CuentaService cuenService;

    public FuncionesAlumno() {
    }
    
    public void agregarAlumno(Alumno a){
        alumService.Guardar(a);
    }
    
    public void agregarCuentaAutomaticamenteAlAlumno(Alumno a){

        Cuenta c=new Cuenta();
        c.setUsuario(a.getRut());
        c.setPassword(a.getRut());
        c.setNivelUsuario(3);
        cuenService.Guardar(c); 
        
        List<Cuenta> cuentas=cuenService.listarTodos();
        
        a.setIdCuenta(cuentas.get(cuentas.size()-1));
        
    }
    
}
