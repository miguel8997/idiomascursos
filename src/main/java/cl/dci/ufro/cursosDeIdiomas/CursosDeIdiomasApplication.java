package cl.dci.ufro.cursosDeIdiomas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursosDeIdiomasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursosDeIdiomasApplication.class, args);
	}
   
}
